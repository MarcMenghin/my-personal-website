# Marc Menghin's Private Webpage Code

This project represents the code for Marc Menghins personal website.

# Based on

[angular-webpack](https://github.com/preboot/angular-webpack) (A complete, yet simple, starter for Angular v2+ using Webpack. Commit on Mar 26, 2017)

# Uses also
[ngx-boogstrap](http://valor-software.com/ngx-bootstrap) (for Bootstrap 4)

# Usage

To Start development server: `npm start`

To update packages use: `ncu` (npm-check-updates)

To build the website use: `npm run build`
