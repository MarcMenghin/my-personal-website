import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app.header.component';
import { AppFooterComponent } from './app.footer.component';
import { ResumeModule } from './component/resume.module';

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

const appRoutes: Routes = [
  { path: '',   redirectTo: 'resume', pathMatch: 'full' },
];

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ResumeModule,
    RouterModule.forRoot(appRoutes),
  ],
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppFooterComponent,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
