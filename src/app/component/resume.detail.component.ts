import { Component, OnInit } from '@angular/core';
import { DetailService } from '../service/detail.service';

@Component({
    selector: 'resume-detail',
    providers: [DetailService],
    templateUrl: './resume.detail.component.html'
})
export class ResumeDetailComponent implements OnInit {
    detailService: DetailService;


    constructor(_detailService: DetailService) {
        this.detailService = _detailService;
        this.detailService.loadData();
    }

    ngOnInit() {

    }
}
