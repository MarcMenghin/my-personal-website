import { Component, OnInit } from '@angular/core';
import { EducationService } from '../service/education.service';

@Component({
    selector: 'resume-education',
    providers: [EducationService],
    templateUrl: './resume.education.component.html'
})
export class ResumeEducationComponent implements OnInit {
    educationService: EducationService;

    constructor(_educationService: EducationService) {
        this.educationService = _educationService;
        this.educationService.loadData();
    }

    ngOnInit() {

    }
}
