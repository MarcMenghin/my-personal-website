import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CarouselModule } from 'ngx-bootstrap/carousel';

import { ResumeView } from './resume.view';
import { ResumeDetailComponent } from './resume.detail.component';
import { ResumeEducationComponent } from './resume.education.component';
import { ResumeExperienceComponent } from './resume.experience.component';
import { ResumeProjectsComponent } from './resume.projects.component';
import { ResumeOtherStuffComponent } from './resume.otherstuff.component';
import { ResumeImagesComponent } from './resume.images.component';
import { SortArrayPipe } from '../pipe/sortarray.pipe';

const resumeRoutes: Routes = [
  { path: 'resume', component: ResumeView },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(resumeRoutes),
    CarouselModule.forRoot(),
  ],
  declarations: [
    ResumeView,
    ResumeDetailComponent,
    ResumeEducationComponent,
    ResumeExperienceComponent,
    ResumeProjectsComponent,
    ResumeOtherStuffComponent,
    ResumeImagesComponent,
    SortArrayPipe
  ],
  exports: [
    RouterModule,
    ResumeView
  ]
})
export class ResumeModule { }
