import { Component, OnInit } from '@angular/core';
import { DetailService } from '../service/detail.service';

@Component({
    selector: 'resume-other-stuff',
    providers: [DetailService],
    templateUrl: './resume.otherstuff.component.html'
})
export class ResumeOtherStuffComponent implements OnInit {
    detailService: DetailService;


    constructor(_detailService: DetailService) {
        this.detailService = _detailService;
        this.detailService.loadData();
    }

    ngOnInit() {

    }
}
