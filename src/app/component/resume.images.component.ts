import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

@Component({
    selector: 'resume-images',
    templateUrl: './resume.images.component.html'
})
export class ResumeImagesComponent implements OnInit {
    public myInterval: number = 8000;
    public noWrapSlides: boolean = false;
    public slides: Array<any> = [
        { 'image': 'img/DSC00359.jpg', 'text': '' },
        { 'image': 'img/DSC00368.jpg', 'text': '' },
        { 'image': 'img/DSC00443.jpg', 'text': '' },
        { 'image': 'img/DSC00466.jpg', 'text': '' },
        { 'image': 'img/DSC00478.jpg', 'text': '' },
        { 'image': 'img/DSC00490.jpg', 'text': '' },
        { 'image': 'img/DSC01820.jpg', 'text': '' },
        { 'image': 'img/DSC02669.jpg', 'text': '' },
        { 'image': 'img/DSC02711.jpg', 'text': '' },
        { 'image': 'img/DSC04417.jpg', 'text': '' },
        { 'image': 'img/DSC04448.jpg', 'text': '' },
        { 'image': 'img/DSC04450.jpg', 'text': '' },
        { 'image': 'img/DSC06399.jpg', 'text': '' },
        { 'image': 'img/DSC06608.jpg', 'text': '' },
        { 'image': 'img/DSC07323.jpg', 'text': '' },
        { 'image': 'img/DSC07671.jpg', 'text': '' },
        { 'image': 'img/DSC07761.jpg', 'text': '' },
        { 'image': 'img/DSC08899.jpg', 'text': '' }
    ];

    ngOnInit() {
        // shuffle array and then pick first 5 to display.
        this.shuffle(this.slides);
        this.slides = this.slides.slice(0, 5);
    }

    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }
}
