import { Component, OnInit } from '@angular/core';
import { ExperienceService } from '../service/experience.service';

@Component({
    selector: 'resume-experience',
    providers: [ExperienceService],
    templateUrl: './resume.experience.component.html'
})
export class ResumeExperienceComponent implements OnInit {
    experienceService: ExperienceService;

    constructor(_experienceService: ExperienceService) {
        this.experienceService = _experienceService;
        this.experienceService.loadData();
    }

    ngOnInit() {

    }
}
