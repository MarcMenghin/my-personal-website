import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../service/projects.service';

@Component({
    selector: 'resume-projects',
    providers: [ProjectsService],
    templateUrl: './resume.projects.component.html',
})
export class ResumeProjectsComponent implements OnInit {
    projectsService: ProjectsService;

    constructor(_projectsService: ProjectsService) {
        this.projectsService = _projectsService;
        this.projectsService.loadData();
    }

    ngOnInit() {

    }
}
