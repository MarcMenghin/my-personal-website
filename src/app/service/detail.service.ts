import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Http } from '@angular/http';

interface Detail {
    programminglanguages: Object[];
}

@Injectable()
export class DetailService {

    _jsonDataUrl = './data/details.json';
    details: Detail;
    private http: Http;

    constructor(_http: Http) {
        this.http = _http;
    }

    loadData() {
        this.http.get(this._jsonDataUrl).map((res: Response) => {
            return res.json();
        }).subscribe(res => {
            // console.log(res);
            this.details = res.details;
        }, err => {
            console.log(err);
        });
    }
}
